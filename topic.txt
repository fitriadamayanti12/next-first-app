Server Side Rendering with Next.js

1. Starting project
2. Putting a list meetups
3. Adding the new meetup form
4. The app.js file layout wrapper
5. Adding custom components & css module
6. Data fetching for staticpage
7. More on static site generator
8. Exploring server side rendering
9. Preparing paths for get static paths
10. Working with MongoDB
11. Sending HTTP request to our API routes
12. Getting data from database
13. Getting meetup details data
14. Final project
15. Deploy with vercel