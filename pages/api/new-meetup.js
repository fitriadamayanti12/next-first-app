import { MongoClient } from "mongodb";

// POST /api/new-meetup

async function handler(req, res) {
  if (req.method === "POST") {
    const data = req.body;

    const client = await MongoClient.connect(
      "mongodb://fitriadamayanti:waLPfpst1u1bKqD8@ac-kzdpvhl-shard-00-00.he6ff4q.mongodb.net:27017,ac-kzdpvhl-shard-00-01.he6ff4q.mongodb.net:27017,ac-kzdpvhl-shard-00-02.he6ff4q.mongodb.net:27017/meetups?ssl=true&replicaSet=atlas-13n6uy-shard-0&authSource=admin&retryWrites=true&w=majority"
    );
    const db = client.db();

    const meetupCollection = db.collection("meetups");

    const result = await meetupCollection.insertOne(data);

    console.log(result);

    client.close();

    res.status(201).json({ message: "Meetup inserted!" });
  }
}

export default handler;
